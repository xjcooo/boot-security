package org.xjcooo.manager.service;

import org.xjcooo.manager.dto.UserDto;
import org.xjcooo.manager.model.SysUser;

public interface UserService {

	SysUser saveUser(UserDto userDto);

	SysUser updateUser(UserDto userDto);

	SysUser getUser(String username);

	void changePassword(String username, String oldPassword, String newPassword);

}
