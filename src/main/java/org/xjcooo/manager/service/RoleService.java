package org.xjcooo.manager.service;

import org.xjcooo.manager.dto.RoleDto;

public interface RoleService {

	void saveRole(RoleDto roleDto);

	void deleteRole(Long id);
}
