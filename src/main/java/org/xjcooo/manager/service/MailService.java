package org.xjcooo.manager.service;

import java.util.List;

import org.xjcooo.manager.model.Mail;

public interface MailService {

	void save(Mail mail, List<String> toUser);
}
