package org.xjcooo.manager.service;

import org.quartz.JobDataMap;
import org.quartz.SchedulerException;

import org.xjcooo.manager.model.JobModel;

public interface JobService {

	void saveJob(JobModel jobModel);

	void doJob(JobDataMap jobDataMap);

	void deleteJob(Long id) throws SchedulerException;
}
