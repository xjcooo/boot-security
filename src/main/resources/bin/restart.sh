#!/bin/bash
ps -ef | grep boot-security | grep -v grep | awk '{print $2}' | xargs kill
nohup java -jar lib/boot-security.jar --spring.config.location=classpath:application.yml > /dev/null 2>&1 &
tailf logs/boot-security.log
